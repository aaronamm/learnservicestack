using System;
using ServiceStack.ServiceHost;

namespace ProteinTrackerServiceServiceStack3.Services
{
    //[Route("/status/{Date}", "GET")]
    [Route("/status", "GET")]
    public class StatusQuery :IReturn<StatusResponse>
    {
        //[ApiMember(Name="Date")]
        public DateTime Date { get; set; }
    }
}