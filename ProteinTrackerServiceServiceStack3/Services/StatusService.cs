﻿using ServiceStack.ServiceInterface;
using log4net;

namespace ProteinTrackerServiceServiceStack3.Services
{
    public class StatusService : Service
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(StatusService).Name);
        public TrackedDataRepository TrackedDataRepository { get; set; }

        public object Get(StatusQuery request)
        {
            _log.Debug("StatusService Get called");
            var trackedData = TrackedDataRepository.Get(request.Date,Cache );
            return new StatusResponse() { Total = trackedData.Total, Goal = trackedData.Goal };
        }

    }
}