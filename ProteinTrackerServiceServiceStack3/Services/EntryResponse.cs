using ServiceStack.ServiceInterface.ServiceModel;

namespace ProteinTrackerServiceServiceStack3.Services
{
    public class EntryResponse
    {
        public int Id { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}