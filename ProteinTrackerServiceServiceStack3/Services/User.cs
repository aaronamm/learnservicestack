﻿using ServiceStack.ServiceHost;

namespace ProteinTrackerServiceServiceStack3.Services
{
    [Route("/User/Register","POST")]
    public class UserRegister
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
    }
}