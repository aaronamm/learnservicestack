using System;
using ServiceStack.ServiceHost;

namespace ProteinTrackerServiceServiceStack3.Services
{
    [Route("/entry","POST")]
    public class Entry:IReturn<EntryResponse>
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public int Amount { get; set; }
    }
}