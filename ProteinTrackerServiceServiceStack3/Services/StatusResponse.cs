using ServiceStack.ServiceInterface.ServiceModel;

namespace ProteinTrackerServiceServiceStack3.Services
{
    public class StatusResponse
    {
        public int Total { get; set; }
        public int Goal { get; set; }

        //exception wired here
        public ResponseStatus ResponseStatus { get; set; }
    }
}