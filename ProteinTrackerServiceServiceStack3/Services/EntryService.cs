﻿using System;
using ServiceStack.CacheAccess;
using ServiceStack.ServiceInterface;
using log4net;

namespace ProteinTrackerServiceServiceStack3.Services
{
    public class EntryService : Service
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(EntryService).Name);
        public TrackedDataRepository TrackedDataRepository { get; set; }

        public object Post(Entry request)
        {
            TrackedDataRepository.Add(request.Time, request.Amount, Cache);
            return new EntryResponse() { Id = 1 };
        }


        public object Get(Entry entry)
        {
            return new EntryResponse() { Id = entry.Id };
        }
    }

    public class TrackedData
    {
        public int Total { get; set; }
        public int Goal { get; set; }
    }


    public class TrackedDataRepository
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(TrackedDataRepository).Name);

        public void Add(DateTime time, int amount, ICacheClient cache)
        {
            try
            {
                var date = time.Date;
                var trackedData = cache.Get<TrackedData>(date.ToString());
                if (trackedData == null)
                {
                    trackedData = new TrackedData() { Goal = 300 };
                }

                trackedData.Total += amount;
                cache.Set(date.ToString(), trackedData);

            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }


        public TrackedData Get(DateTime time,ICacheClient cache)
        {
            TrackedData trackedData;
            try
            {
                var date = time.Date.Date;
                trackedData = cache.Get<TrackedData>(date.ToString());
                if (trackedData == null)
                {
                    trackedData = new TrackedData() { Goal = 300, Total = 0 };
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                trackedData = new TrackedData();
            }
            return trackedData;
        }
    }
}