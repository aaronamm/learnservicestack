﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProteinTrackerServiceServiceStack3.Services;
using ServiceStack;
using ServiceStack.ServiceClient.Web;

namespace ProteinTrackerService.Client
{
    //Install-Package ServiceStack.Client
    class Program
    {
        static void Main(string[] args)
        {

            var client = new JsonServiceClient("http://localhost:1234/api");

            var amount = 1;
            while (amount != 0)
            {
                Console.WriteLine("Enter protein amount (0 to exit):");
                amount = int.Parse(Console.ReadLine());

                //call the service and add an entry
                var response = client.Send(new Entry() { Amount = amount, Time = DateTime.Now });
                Console.WriteLine("Response: {0}", response);
            }

            var statusQuery = new StatusQuery() { Date = DateTime.Now };
            var statusResponse = client.Get(statusQuery);
            Console.WriteLine("{0} / {1}", statusResponse.Total, statusResponse.Goal);
        }
    }
}
